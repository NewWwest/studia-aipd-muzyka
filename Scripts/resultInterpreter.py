#!/usr/bin/env python3
import json
import numpy as np
import matplotlib.pyplot as plt
import math
import sys

PLOTS_TOTAL=11
LOGGING_ENABLED=True

def eliminate_nulls(array):
    for i in range(0, len(array)):
        try:
            float(array[i])
        except ValueError:
            array[i]=0
        
        try:
            if array[i] == 'NaN' or array[i] == 'Infinity' or array[i] == '-Infinity':
                array[i]=0
        except TypeError:
            print(type(array[i]))
            print(array[i])

def legend_for(data, outfile):
    legend=[]

    for p in data:
        if LOGGING_ENABLED:
            print(p['Filename'])

        plt.plot(1)
        legend.append(p['Filename'])

    plt.legend(legend,loc='upper right') 
    plt.savefig(outfile + ".legend.png")


def plot_for(data, propertyName, index):
    plt.subplot(PLOTS_TOTAL, 1, index)
    if LOGGING_ENABLED:
        print(propertyName)
    for p in data:
        if LOGGING_ENABLED:
            print(p['Filename'])

        array=p[propertyName]
        eliminate_nulls(array)
        plt.plot(p[propertyName])

    plt.title(propertyName)


def process_file(filename, outfile):
    with open(filename) as json_file: 
        data = json.load(json_file)

        legend_for(data, outfile)

        plt.figure(num=None, figsize=(8, PLOTS_TOTAL*3), dpi=100, facecolor='w', edgecolor='k')

        plot_for(data, 'ZeroCrossingRate', 1)
        plot_for(data, 'ShortTimeEnergy', 2)
        plot_for(data, 'Loudness', 3)
        plot_for(data, 'FrequencyCentroid', 4)
        plot_for(data, 'EffectiveBandwidth', 5)
        plot_for(data, 'BandEnergy', 6)
        plot_for(data, 'ERSB1', 7)
        plot_for(data, 'ERSB2', 8)
        plot_for(data, 'ERSB3', 9)
        plot_for(data, 'SpectralFlatnessMeasure', 10)
        plot_for(data, 'SpectralCrestFactor', 11)
        plt.subplots_adjust(wspace = 0.3)

        plt.tight_layout()
        plt.savefig(outfile)

if __name__ == "__main__":
    print('input file:', sys.argv[1])
    print('output file:', sys.argv[2])
    process_file(sys.argv[1], sys.argv[2])