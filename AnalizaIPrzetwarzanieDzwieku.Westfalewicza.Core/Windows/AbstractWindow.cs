﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnalizaIPrzetwarzanieDzwieku.Westfalewicza.Core.Windows
{
    public abstract class AbstractWindow
    {
        public int SampleCount { get; private set; }

        public AbstractWindow(int count)
        {
            SampleCount = count;
        }

        public abstract double Calculate(int index, double value);

        public  double Integral()
        {
            double result = 0;
            for (int i = 0; i < SampleCount; i++)
            {
                result += Calculate(i, 1);
            }
            return result;
        }
    }
}
