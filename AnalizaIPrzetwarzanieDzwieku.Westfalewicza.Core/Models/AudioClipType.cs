﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnalizaIPrzetwarzanieDzwieku.Westfalewicza.Core.Models
{
    public enum AudioClipType
    {
        None,
        Music,
        Speech,
    }
}
