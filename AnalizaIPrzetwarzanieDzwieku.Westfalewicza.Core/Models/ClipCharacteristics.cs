﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WestfalewiczaAipdProjekt1.Core.Models
{
    public class ClipCharacteristics
    {
        public string Filename { get; set; }
        public Channel Channel { get; set; }
        public float Start { get; set; }
        public float End { get; set; }

        public float LowShortTimeEnergyRatio { get; set; }
        public float ZeroCrossingRateStandardDeviation { get; set; }

        public float[] ZeroCrossingRate { get; set; }
        public float[] ShortTimeEnergy { get; set; }
                    
        public float[] Loudness { get; set; }
        public float[] FrequencyCentroid { get; set; }
        public float[] EffectiveBandwidth { get; set; }
        public float[] BandEnergy { get; set; }
        public float[] ERSB1 { get; set; }
        public float[] ERSB2 { get; set; }
        public float[] ERSB3 { get; set; }
        public float[] SpectralFlatnessMeasure { get; set; }
        public float[] SpectralCrestFactor { get; set; }
    }
}
