﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AnalizaIPrzetwarzanieDzwieku.Westfalewicza.Core.Utils;

namespace AnalizaIPrzetwarzanieDzwieku.Westfalewicza.Core.Models
{
    public class AudioFrame
    {
        public List<float> Data { get; set; }
        public int StartIndex { get; set; }
        public int EndIndex { get; set; }
        public float Start { get; set; }
        public float End { get; set; }

        private Lazy<float> _zcr => new Lazy<float>(ZcrFactory);
        private Lazy<float> _ste => new Lazy<float>(SteFactory);
        private Lazy<AudioFrameType> _frameType => new Lazy<AudioFrameType>(FrameTypeFactory);

        private float ZcrFactory() => new FrameLevelProcessing().ZeroCrossingRate(this);
        private float SteFactory() => new FrameLevelProcessing().ShortTimeEnergy(this);

        public float Zcr => _zcr.Value;
        public float Ste => _ste.Value;
        public AudioFrameType FrameType => _frameType.Value;

        public AudioFrame(List<float> Data, int StartIndex, int EndIndex, float Start, float End)
        {
            this.Data = Data;
            this.StartIndex = StartIndex;
            this.Start = Start;
            this.End = End;
            this.EndIndex = EndIndex;
        }
        
        public void Analize()
        {
            var temp2 = FrameType;
        }

        private AudioFrameType FrameTypeFactory()
        {
            AudioFrameType type = AudioFrameType.None;
            if (isSilence(Zcr, Ste))
                type = AudioFrameType.Silence;
            else if (isUnvoiced(Zcr, Ste))
                type = AudioFrameType.Unvoiced;
            else if (isVoiced(Zcr, Ste))
                type = AudioFrameType.Voiced;

            return type;
        }

        private bool isSilence(float zcr, float ste) =>
            zcr <= Config.SilenceZcrThreshold &&
            ste <= Config.SilenceSteThreshold;

        private bool isUnvoiced(float zcr, float ste) =>
            zcr >= Config.UnvoicedZcrThreshold &&
            ste <= Config.UnvoicedSteThreshold &&
            zcr / ste > Config.UnvoicedZcr2SteThreshold;

        private bool isVoiced(float zcr, float ste) =>
            zcr <= Config.VoicedZcrThreshold &&
            ste >= Config.SilenceSteThreshold;
    }
}
