﻿using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnalizaIPrzetwarzanieDzwieku.Westfalewicza.Core.Models
{
    [Serializable()]
    public class Audio
    {
        public string Filename { get; set; }
        public int SampleRate { get; set; }
        public List<float> Left { get; set; }
        public List<float> Right { get; set; }
        public float Time { get; set; }

        public Audio(string filename, int sampleRate, List<float> left, List<float> right, float time)
        {
            Filename = filename;
            SampleRate = sampleRate;
            Left = left;
            Right = right;
            Time = time;
        }

        protected Audio(Audio audio)
        {
            Filename = audio.Filename;
            SampleRate = audio.SampleRate;
            Left = audio.Left;
            Right = audio.Right;
            Time = audio.Time;
        }
    }
}
