﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace AnalizaIPrzetwarzanieDzwieku.Westfalewicza.Core.Utils
{
    public static class DebuggingTools
    {
        public static long ObjectSize(object instance)
        {
            using (Stream s = new MemoryStream())
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(s, instance);
                return  s.Length;
            }
        }
    }
}
