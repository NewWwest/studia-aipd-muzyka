﻿using AnalizaIPrzetwarzanieDzwieku.Westfalewicza.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnalizaIPrzetwarzanieDzwieku.Westfalewicza.Core.Utils
{
    public class ClipLevelProcessing
    {
        public float LowShortTimeEnergyRatio(List<AudioFrame> frames)
        {
            float avg = frames.Average(x => x.Ste);
            float sum = frames.Sum(x => Math.Sign(0.2f * avg - x.Ste) + 1);

            return 0.5f * sum / frames.Count;
        }

        public float ZeroCrossingRateStandardDeviation(List<AudioFrame> frames)
        {
            float avg = frames.Average(x => x.Zcr);
            float sum = frames.Sum(x => (x.Zcr - avg) * (x.Zcr - avg));

            return (float)Math.Sqrt(sum / frames.Count);
        }
    }
}
