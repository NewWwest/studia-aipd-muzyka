﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using AnalizaIPrzetwarzanieDzwieku.Westfalewicza.Core.Models;

namespace AnalizaIPrzetwarzanieDzwieku.Westfalewicza.Core.Utils
{
    public partial class FrameLevelProcessing
    {
        public float ZeroCrossingRate(AudioFrame frame)
        {
            float signChangeAcc = 0f;
            for (int i = 1; i < frame.Data.Count; i++)
            {
                signChangeAcc += Math.Abs(Math.Sign(frame.Data[i]) - Math.Sign(frame.Data[i - 1]));
            }
            return signChangeAcc * 0.5f / frame.Data.Count;
        }

        public float ShortTimeEnergy(AudioFrame frame)
        {
            return (frame.Data.Sum(x => x * x) / frame.Data.Count);
        }

        public float FrequencyCentroid(IEnumerable<KeyValuePair<double, Complex>> frequencySpectrum)
        {
            double top = 0;
            double bot = 0;

            foreach (var kv in frequencySpectrum)
            {
                top += kv.Key * kv.Value.Magnitude;
                bot += kv.Value.Magnitude;
            }
            return (float)(top / bot);
        }

        public float EffectiveBandwidth(IEnumerable<KeyValuePair<double, Complex>> frequencySpectrum, double frequencyCentroid)
        {
            double top = 0;
            double bot = 0;

            foreach (var kv in frequencySpectrum)
            {
                top += (kv.Key - frequencyCentroid) * (kv.Key - frequencyCentroid) * (kv.Value.Magnitude) * kv.Value.Magnitude;
                bot += kv.Value.Magnitude * kv.Value.Magnitude;
            }
            return (float)(top / bot);
        }

        public float SpectralFlatnessMeasure(IEnumerable<KeyValuePair<double, Complex>> frequencySpectrum)
        {
            double geoSum = 1;
            double ariSum = 0;
            int n = frequencySpectrum.Count();
            foreach (var kv in frequencySpectrum)
            {
                if (kv.Value.Magnitude == 0 || double.IsInfinity(kv.Value.Magnitude))
                    continue;
                geoSum *= Math.Pow(kv.Value.Magnitude * kv.Value.Magnitude, 1f / n);
                ariSum += kv.Value.Magnitude * kv.Value.Magnitude;
            }
            var SFM = geoSum / (ariSum / n);
            return (float)SFM;
        }

        public float SpectralCrestFactor(IEnumerable<KeyValuePair<double, Complex>> frequencySpectrum)
        {
            double max = double.MinValue;
            double ariSum = 0;

            foreach (var kv in frequencySpectrum)
            {
                if (max < kv.Value.Magnitude)
                    max = kv.Value.Magnitude;

                ariSum += kv.Value.Magnitude * kv.Value.Magnitude;
            }

            return (float)(max / ariSum / frequencySpectrum.Count());
        }
    }
}
