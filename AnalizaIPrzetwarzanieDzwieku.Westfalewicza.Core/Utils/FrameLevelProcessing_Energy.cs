﻿using AnalizaIPrzetwarzanieDzwieku.Westfalewicza.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace AnalizaIPrzetwarzanieDzwieku.Westfalewicza.Core.Utils
{
    public partial class FrameLevelProcessing
    {
        public float BandEnergy(IEnumerable<KeyValuePair<double, Complex>> frequencySpectrum, float windowIntegral)
        {
            double result = 0;
            foreach (var kv in frequencySpectrum)
            {
                result += kv.Value.Magnitude * kv.Value.Magnitude;
            }
            return (float)(result / windowIntegral);
        }

        public float BandEnergyRatio(float bandEnergy, float Loudness)
        {
            return bandEnergy / Loudness;
        }

        public float ERSB1(IEnumerable<KeyValuePair<double, Complex>> frequencySpectrum, float windowIntegral, float Loudness)
        {
            var limitedSpectrum = frequencySpectrum.Where(kv => Config.ErsB1BoottomFrequency < kv.Key && kv.Key < Config.ErsB1TopFrequency);
            var bandEnergy = BandEnergy(limitedSpectrum, windowIntegral);
            return BandEnergyRatio(bandEnergy, Loudness);
        }

        public float ERSB2(IEnumerable<KeyValuePair<double, Complex>> frequencySpectrum, float windowIntegral, float Loudness)
        {
            var limitedSpectrum = frequencySpectrum.Where(kv => Config.ErsB2BoottomFrequency < kv.Key && kv.Key < Config.ErsB2TopFrequency);
            var bandEnergy = BandEnergy(limitedSpectrum, windowIntegral);
            return BandEnergyRatio(bandEnergy, Loudness);

        }

        public float ERSB3(IEnumerable<KeyValuePair<double, Complex>> frequencySpectrum, float windowIntegral, float Loudness)
        {
            var limitedSpectrum = frequencySpectrum.Where(kv => Config.ErsB3BoottomFrequency < kv.Key && kv.Key < Config.ErsB3TopFrequency);
            var bandEnergy = BandEnergy(limitedSpectrum, windowIntegral);
            return BandEnergyRatio(bandEnergy, Loudness);
        }

        public float ERSB4(IEnumerable<KeyValuePair<double, Complex>> frequencySpectrum, float windowIntegral, float Loudness)
        {
            var limitedSpectrum = frequencySpectrum.Where(kv => Config.ErsB4BoottomFrequency < kv.Key && kv.Key < Config.ErsB4TopFrequency);
            var bandEnergy = BandEnergy(limitedSpectrum, windowIntegral);
            return BandEnergyRatio(bandEnergy, Loudness);
        }
    }
}
