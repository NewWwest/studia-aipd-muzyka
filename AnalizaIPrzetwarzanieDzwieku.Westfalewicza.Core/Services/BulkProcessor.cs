﻿using AnalizaIPrzetwarzanieDzwieku.Westfalewicza.Core.FourierTransforms;
using AnalizaIPrzetwarzanieDzwieku.Westfalewicza.Core.Models;
using AnalizaIPrzetwarzanieDzwieku.Westfalewicza.Core.Utils;
using AnalizaIPrzetwarzanieDzwieku.Westfalewicza.Core.Windows;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using WestfalewiczaAipdProjekt1.Core.Models;

namespace AnalizaIPrzetwarzanieDzwieku.Westfalewicza.Core.Services
{
    public class BulkProcessor
    {
        public IEnumerable<ClipCharacteristics> DoProcess(RandomTimeAccessAudio audio, AbstractWindow window, int secondFrom, int secondsFor, int overlay, int frameLength, bool onlyLeftChannel = false)
        {
            var result = new List<ClipCharacteristics>();
            (IEnumerable<AudioFrame>, IEnumerable<AudioFrame>) frames = audio.Frames(secondFrom, secondsFor, overlay, frameLength);
            var left = DoProcessChannel(frames.Item1.ToList(), Channel.Left, audio.Filename, window, audio.SampleRate, secondFrom, secondsFor, overlay, frameLength);
            result.Add(left);
            if (!onlyLeftChannel)
            {
                var right = DoProcessChannel(frames.Item2.ToList(), Channel.Right, audio.Filename, window, audio.SampleRate, secondFrom, secondsFor, overlay, frameLength);
                result.Add(right);
            }
            return result;
        }

        private ClipCharacteristics DoProcessChannel(List<AudioFrame> frames, Channel channel, string filename, AbstractWindow window, int sampleRate, int secondFrom, int secondsFor, int overlay, int frameLength)
        {
            var xd = new ClipCharacteristics();
            xd.Filename = Path.GetFileName(filename);
            xd.Start = secondFrom;
            xd.End = secondFrom + secondsFor;
            xd.Channel = channel;

            var clipProcessor = new ClipLevelProcessing();
            var framePrcessor = new FrameLevelProcessing();

            var zeroCrossingRate = new List<float>(frames.Count);
            var shortTimeEnergy = new List<float>(frames.Count);
            var loudness = new List<float>(frames.Count);
            var frequencyCentroid = new List<float>(frames.Count);
            var effectiveBandwidth = new List<float>(frames.Count);
            var bandEnergy = new List<float>(frames.Count);
            var ERSB1 = new List<float>(frames.Count);
            var ERSB2 = new List<float>(frames.Count);
            var ERSB3 = new List<float>(frames.Count);
            var spectralFlatnessMeasure = new List<float>(frames.Count);
            var spectralCrestFactor = new List<float>(frames.Count);

            xd.LowShortTimeEnergyRatio = clipProcessor.LowShortTimeEnergyRatio(frames);
            xd.ZeroCrossingRateStandardDeviation = clipProcessor.ZeroCrossingRateStandardDeviation(frames);

            var processor = new FrequencyAnalizer();
            var frequencies = processor.FrequenciesInHearableRange(frames, sampleRate, FourierProxy.Fourier(), window);

            for (int i = 0; i < frames.Count; i++)
            {
                AudioFrame frame = frames[i];
                var frequenciesMapped = frequencies.Select(kv => new KeyValuePair<double, Complex>(kv.Key, kv.Value[i])).ToList();

                zeroCrossingRate.Add(framePrcessor.ZeroCrossingRate(frame));
                shortTimeEnergy.Add(framePrcessor.ShortTimeEnergy(frame));
                loudness.Add(framePrcessor.BandEnergy(frequenciesMapped, (float)window.Integral()));

                frequencyCentroid.Add(framePrcessor.FrequencyCentroid(frequenciesMapped));
                effectiveBandwidth.Add(framePrcessor.EffectiveBandwidth(frequenciesMapped, frequencyCentroid[i]));
                bandEnergy.Add(framePrcessor.BandEnergy(frequenciesMapped, (float)window.Integral()));
                ERSB1.Add(framePrcessor.ERSB1(frequenciesMapped, (float)window.Integral(), loudness[i]));
                ERSB2.Add(framePrcessor.ERSB2(frequenciesMapped, (float)window.Integral(), loudness[i]));
                ERSB3.Add(framePrcessor.ERSB3(frequenciesMapped, (float)window.Integral(), loudness[i]));
                spectralFlatnessMeasure.Add(framePrcessor.SpectralFlatnessMeasure(frequenciesMapped));
                spectralCrestFactor.Add(framePrcessor.SpectralCrestFactor(frequenciesMapped));
            }

            xd.ZeroCrossingRate = zeroCrossingRate.ToArray();
            xd.ShortTimeEnergy = shortTimeEnergy.ToArray();
            xd.Loudness = loudness.ToArray();
            xd.FrequencyCentroid = frequencyCentroid.ToArray();
            xd.EffectiveBandwidth = effectiveBandwidth.ToArray();
            xd.BandEnergy = bandEnergy.ToArray();
            xd.ERSB1 = ERSB1.ToArray();
            xd.ERSB2 = ERSB2.ToArray();
            xd.ERSB3 = ERSB3.ToArray();
            xd.SpectralFlatnessMeasure = spectralFlatnessMeasure.ToArray();
            xd.SpectralCrestFactor = spectralCrestFactor.ToArray();
            return xd;
        }
    }
}
