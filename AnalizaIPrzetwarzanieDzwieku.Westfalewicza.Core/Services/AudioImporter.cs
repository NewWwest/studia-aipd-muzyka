﻿using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AnalizaIPrzetwarzanieDzwieku.Westfalewicza.Core.Models;

namespace AnalizaIPrzetwarzanieDzwieku.Westfalewicza.Core.Services
{
    public class AudioImporter
    {

        public Audio Import(string filename)
        {
            var reader = new AudioFileReader(filename);
            
            float[] buffer = new float[reader.Length / 4];

            int alreadyRead = 0;
            while ((alreadyRead = reader.Read(buffer, 0, buffer.Length)) != 0) ;

            List<float> leftChannel = null;
            List<float> rightChannel = null;

            if (reader.WaveFormat.Channels == 1)
            {
                leftChannel = buffer.ToList();
                rightChannel = leftChannel;

            }
            else
            {
                leftChannel = new List<float>(buffer.Length / 2);
                rightChannel = new List<float>(buffer.Length / 2);
                for (int i = 0; i < buffer.Length; i++)
                {
                    if (i % 2 == 0)
                    {
                        leftChannel.Add(buffer[i]);
                    }
                    else
                    {
                        rightChannel.Add(buffer[i]);
                    }
                }
            }

            var xd = new Audio(
                filename,
                reader.WaveFormat.SampleRate,
                leftChannel,
                rightChannel,
                (float)leftChannel.Count / reader.WaveFormat.SampleRate
            );
            return xd;
        }
    }
}
