﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnalizaIPrzetwarzanieDzwieku.Westfalewicza.Core.FourierTransforms
{
    public static class FourierProxy
    {
        public static IFourierTransform Fourier()
        {
            return new CustomFourier();
        }
    }
}
