﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AnalizaIPrzetwarzanieDzwieku.Westfalewicza.Core.Models;
using AnalizaIPrzetwarzanieDzwieku.Westfalewicza.Core.Windows;
using System.Diagnostics;
using AnalizaIPrzetwarzanieDzwieku.Westfalewicza.Core;
using System.IO;
using AnalizaIPrzetwarzanieDzwieku.Westfalewicza.Core.Services;
using WestfalewiczaAipdProjekt1.Core.Models;
using AnalizaIPrzetwarzanieDzwieku.Westfalewicza.Core.Utils;
using AnalizaIPrzetwarzanieDzwieku.Westfalewicza.Core.FourierTransforms;
using System.Numerics;
using System.Collections.Concurrent;
using Newtonsoft.Json;
using CsvHelper;
using CsvHelper.Configuration;

namespace WestfalewiczaAipdProjekt1.Controls
{
    public partial class ClipCharacteristicControl : UserControl
    {
        private readonly List<string> EXTENSIONS = new List<string>() { ".mp3", ".wav", ".aiff", ".wma" };
        private string[] _files = null;

        public ClipCharacteristicControl()
        {
            InitializeComponent();
        }

        private void ProcessButton_Click(object sender, EventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();
                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    _files = Directory.GetFiles(fbd.SelectedPath);

                    ProcessingBar.Maximum = 100;
                    ProcessingBar.Step = 1;
                    ProcessingBar.Value = 0;
                    ProcesssingWorker.RunWorkerAsync();
                }
            }
        }


        private void ProcesssingWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            var allResults = new List<ClipCharacteristics>();
            int i = 0;
            foreach (var filename in _files)
            {
                ProcessingLabel.Invoke((MethodInvoker)delegate {ProcessingLabel.Text = filename;});

                if (EXTENSIONS.Any(ext => filename.EndsWith(ext)))
                {
                    var xd1 = new AudioImporter();
                    var dx1 = xd1.Import(filename);

                    var audio = new RandomTimeAccessAudio(dx1);

                    var xd = ProcessAudio(audio);
                    allResults.AddRange(xd);

                    SaveProgress(allResults);

                    audio = null;
                    dx1 = null;
                    xd1 = null;

                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                }
                int percentage = (int)((float)++i / _files.Count() * 100);
                ProcesssingWorker.ReportProgress(percentage);
            }
        }

        private void ProcesssingWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            ProcessingBar.Value = e.ProgressPercentage;
        }

        private void ProcesssingWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            ProcessingBar.Value = 0;
            ProcessingLabel.Text = "Done";
        }

        private IEnumerable<ClipCharacteristics> ProcessAudio(RandomTimeAccessAudio audio)
        {
            int secondFrom = -1, secondsFor = -1, frameLength = -1, overlay = -1;
            try
            {
                if (!int.TryParse(ClipFrom.Text, out secondFrom) || secondFrom < 0 || secondFrom > audio.Time)
                {
                    secondFrom = 0;
                }
            }
            catch (Exception exc)
            {
                Debug.WriteLine(exc);
            }
            try
            {
                if (!int.TryParse(ClipFor.Text, out secondsFor) || secondsFor < 0 || secondsFor + secondFrom > audio.Time)
                {
                    secondsFor = Config.ClipLength < audio.Time ? Config.ClipLength : (int)audio.Time;
                }
            }
            catch (Exception exc)
            {
                Debug.WriteLine(exc);
            }
            try
            {
                if (!int.TryParse(ClipOverlayPercentage.Text, out overlay) || overlay < 0 || overlay > 100)
                {
                    overlay = 0;
                }
            }
            catch (Exception exc)
            {
                Debug.WriteLine(exc);
            }
            try
            {
                if (!int.TryParse(ClipFrameCount.Text, out frameLength) || frameLength < Config.CepstrumFrameLength)
                {
                    frameLength = Config.CepstrumFrameLength;
                }
                frameLength = ClosestPowerOfTwo(frameLength);
            }
            catch (Exception exc)
            {
                Debug.WriteLine(exc);
            }


            BulkProcessor xd = new BulkProcessor();
            return xd.DoProcess(audio, GetFrequencyWindow(), secondFrom, secondsFor, overlay, frameLength, OneChannelCheckbox.Checked);
        }

        private AbstractWindow GetFrequencyWindow()
        {
            if (BarlettWindowRadioButton.Checked)
                return new BarlettWindow(1);
            if (BlackmanWindowRadioButton.Checked)
                return new BlackmanWindow(1);
            if (HammingWindowRadioButton.Checked)
                return new HammingWindow(1);
            if (HannWindowRadioButton.Checked)
                return new HannWindow(1);
            if (RectangularWindowRadioButton.Checked)
                return new RectangularWindow(1);

            throw new Exception();
        }

        private int ClosestPowerOfTwo(int number)
        {
            int current = 2 * 256;
            int prev = 256;
            while (true)
            {
                if (current > number)
                {
                    return Math.Abs(current - number) > Math.Abs(number - prev) ? prev : current;
                }
                prev = current;
                current *= 2;
            }
        }

        private void SaveProgress(List<ClipCharacteristics> data)
        {
            using (FileStream fs = new FileStream("result.json", FileMode.Create))
            using (StreamWriter writer = new StreamWriter(fs))
            {
                writer.Write(JsonConvert.SerializeObject(data));
            }
        }
    }
}
