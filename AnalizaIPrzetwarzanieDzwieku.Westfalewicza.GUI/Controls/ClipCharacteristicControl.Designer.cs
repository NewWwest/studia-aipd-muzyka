﻿namespace WestfalewiczaAipdProjekt1.Controls
{
    partial class ClipCharacteristicControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.FrequencyAnalizingWindowSettings = new System.Windows.Forms.GroupBox();
            this.OneChannelCheckbox = new System.Windows.Forms.CheckBox();
            this.ClipFrameCount = new System.Windows.Forms.TextBox();
            this.ClipFrameCountLabel = new System.Windows.Forms.Label();
            this.ClipOverlayPercentage = new System.Windows.Forms.TextBox();
            this.ProcessButton = new System.Windows.Forms.Button();
            this.ClipForLabel = new System.Windows.Forms.Label();
            this.ClipOverlayPercentageLabel = new System.Windows.Forms.Label();
            this.ClipFromLabel = new System.Windows.Forms.Label();
            this.ClipFrom = new System.Windows.Forms.TextBox();
            this.ClipFor = new System.Windows.Forms.TextBox();
            this.FrequencyWindowBox = new System.Windows.Forms.GroupBox();
            this.BarlettWindowRadioButton = new System.Windows.Forms.RadioButton();
            this.BlackmanWindowRadioButton = new System.Windows.Forms.RadioButton();
            this.HammingWindowRadioButton = new System.Windows.Forms.RadioButton();
            this.HannWindowRadioButton = new System.Windows.Forms.RadioButton();
            this.RectangularWindowRadioButton = new System.Windows.Forms.RadioButton();
            this.ProcessingBar = new System.Windows.Forms.ProgressBar();
            this.ProcessingLabel = new System.Windows.Forms.Label();
            this.ProcesssingWorker = new System.ComponentModel.BackgroundWorker();
            this.FrequencyAnalizingWindowSettings.SuspendLayout();
            this.FrequencyWindowBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // FrequencyAnalizingWindowSettings
            // 
            this.FrequencyAnalizingWindowSettings.Controls.Add(this.OneChannelCheckbox);
            this.FrequencyAnalizingWindowSettings.Controls.Add(this.ClipFrameCount);
            this.FrequencyAnalizingWindowSettings.Controls.Add(this.ClipFrameCountLabel);
            this.FrequencyAnalizingWindowSettings.Controls.Add(this.ClipOverlayPercentage);
            this.FrequencyAnalizingWindowSettings.Controls.Add(this.ProcessButton);
            this.FrequencyAnalizingWindowSettings.Controls.Add(this.ClipForLabel);
            this.FrequencyAnalizingWindowSettings.Controls.Add(this.ClipOverlayPercentageLabel);
            this.FrequencyAnalizingWindowSettings.Controls.Add(this.ClipFromLabel);
            this.FrequencyAnalizingWindowSettings.Controls.Add(this.ClipFrom);
            this.FrequencyAnalizingWindowSettings.Controls.Add(this.ClipFor);
            this.FrequencyAnalizingWindowSettings.Location = new System.Drawing.Point(3, 3);
            this.FrequencyAnalizingWindowSettings.Name = "FrequencyAnalizingWindowSettings";
            this.FrequencyAnalizingWindowSettings.Size = new System.Drawing.Size(347, 179);
            this.FrequencyAnalizingWindowSettings.TabIndex = 2;
            this.FrequencyAnalizingWindowSettings.TabStop = false;
            this.FrequencyAnalizingWindowSettings.Text = "Set window start/leght";
            // 
            // OneChannelCheckbox
            // 
            this.OneChannelCheckbox.AutoSize = true;
            this.OneChannelCheckbox.Location = new System.Drawing.Point(134, 142);
            this.OneChannelCheckbox.Name = "OneChannelCheckbox";
            this.OneChannelCheckbox.Size = new System.Drawing.Size(143, 21);
            this.OneChannelCheckbox.TabIndex = 7;
            this.OneChannelCheckbox.Text = "Only Left Channel";
            this.OneChannelCheckbox.UseVisualStyleBackColor = true;
            // 
            // ClipFrameCount
            // 
            this.ClipFrameCount.Location = new System.Drawing.Point(216, 78);
            this.ClipFrameCount.Name = "ClipFrameCount";
            this.ClipFrameCount.Size = new System.Drawing.Size(100, 22);
            this.ClipFrameCount.TabIndex = 6;
            // 
            // ClipFrameCountLabel
            // 
            this.ClipFrameCountLabel.AutoSize = true;
            this.ClipFrameCountLabel.Location = new System.Drawing.Point(13, 83);
            this.ClipFrameCountLabel.Name = "ClipFrameCountLabel";
            this.ClipFrameCountLabel.Size = new System.Drawing.Size(136, 17);
            this.ClipFrameCountLabel.TabIndex = 5;
            this.ClipFrameCountLabel.Text = "Frame sample count";
            // 
            // ClipOverlayPercentage
            // 
            this.ClipOverlayPercentage.Location = new System.Drawing.Point(216, 106);
            this.ClipOverlayPercentage.Name = "ClipOverlayPercentage";
            this.ClipOverlayPercentage.Size = new System.Drawing.Size(100, 22);
            this.ClipOverlayPercentage.TabIndex = 1;
            // 
            // ProcessButton
            // 
            this.ProcessButton.Location = new System.Drawing.Point(16, 140);
            this.ProcessButton.Name = "ProcessButton";
            this.ProcessButton.Size = new System.Drawing.Size(103, 23);
            this.ProcessButton.TabIndex = 4;
            this.ProcessButton.Text = "Process";
            this.ProcessButton.UseVisualStyleBackColor = true;
            this.ProcessButton.Click += new System.EventHandler(this.ProcessButton_Click);
            // 
            // ClipForLabel
            // 
            this.ClipForLabel.AutoSize = true;
            this.ClipForLabel.Location = new System.Drawing.Point(13, 55);
            this.ClipForLabel.Name = "ClipForLabel";
            this.ClipForLabel.Size = new System.Drawing.Size(87, 17);
            this.ClipForLabel.TabIndex = 3;
            this.ClipForLabel.Text = "Clip duration";
            // 
            // ClipOverlayPercentageLabel
            // 
            this.ClipOverlayPercentageLabel.AutoSize = true;
            this.ClipOverlayPercentageLabel.Location = new System.Drawing.Point(13, 111);
            this.ClipOverlayPercentageLabel.Name = "ClipOverlayPercentageLabel";
            this.ClipOverlayPercentageLabel.Size = new System.Drawing.Size(187, 17);
            this.ClipOverlayPercentageLabel.TabIndex = 2;
            this.ClipOverlayPercentageLabel.Text = "Frames overlay(percentage)";
            // 
            // ClipFromLabel
            // 
            this.ClipFromLabel.AutoSize = true;
            this.ClipFromLabel.Location = new System.Drawing.Point(13, 27);
            this.ClipFromLabel.Name = "ClipFromLabel";
            this.ClipFromLabel.Size = new System.Drawing.Size(116, 17);
            this.ClipFromLabel.TabIndex = 2;
            this.ClipFromLabel.Text = "Process clip from";
            // 
            // ClipFrom
            // 
            this.ClipFrom.Location = new System.Drawing.Point(216, 22);
            this.ClipFrom.Name = "ClipFrom";
            this.ClipFrom.Size = new System.Drawing.Size(100, 22);
            this.ClipFrom.TabIndex = 1;
            // 
            // ClipFor
            // 
            this.ClipFor.Location = new System.Drawing.Point(216, 50);
            this.ClipFor.Name = "ClipFor";
            this.ClipFor.Size = new System.Drawing.Size(100, 22);
            this.ClipFor.TabIndex = 0;
            // 
            // FrequencyWindowBox
            // 
            this.FrequencyWindowBox.Controls.Add(this.BarlettWindowRadioButton);
            this.FrequencyWindowBox.Controls.Add(this.BlackmanWindowRadioButton);
            this.FrequencyWindowBox.Controls.Add(this.HammingWindowRadioButton);
            this.FrequencyWindowBox.Controls.Add(this.HannWindowRadioButton);
            this.FrequencyWindowBox.Controls.Add(this.RectangularWindowRadioButton);
            this.FrequencyWindowBox.Location = new System.Drawing.Point(356, 3);
            this.FrequencyWindowBox.Name = "FrequencyWindowBox";
            this.FrequencyWindowBox.Size = new System.Drawing.Size(347, 179);
            this.FrequencyWindowBox.TabIndex = 4;
            this.FrequencyWindowBox.TabStop = false;
            this.FrequencyWindowBox.Text = "Selection Window";
            // 
            // BarlettWindowRadioButton
            // 
            this.BarlettWindowRadioButton.AutoSize = true;
            this.BarlettWindowRadioButton.Location = new System.Drawing.Point(6, 129);
            this.BarlettWindowRadioButton.Name = "BarlettWindowRadioButton";
            this.BarlettWindowRadioButton.Size = new System.Drawing.Size(119, 21);
            this.BarlettWindowRadioButton.TabIndex = 4;
            this.BarlettWindowRadioButton.Text = "Barlett window";
            this.BarlettWindowRadioButton.UseVisualStyleBackColor = true;
            // 
            // BlackmanWindowRadioButton
            // 
            this.BlackmanWindowRadioButton.AutoSize = true;
            this.BlackmanWindowRadioButton.Location = new System.Drawing.Point(6, 102);
            this.BlackmanWindowRadioButton.Name = "BlackmanWindowRadioButton";
            this.BlackmanWindowRadioButton.Size = new System.Drawing.Size(139, 21);
            this.BlackmanWindowRadioButton.TabIndex = 3;
            this.BlackmanWindowRadioButton.Text = "Blackman window";
            this.BlackmanWindowRadioButton.UseVisualStyleBackColor = true;
            // 
            // HammingWindowRadioButton
            // 
            this.HammingWindowRadioButton.AutoSize = true;
            this.HammingWindowRadioButton.Location = new System.Drawing.Point(6, 75);
            this.HammingWindowRadioButton.Name = "HammingWindowRadioButton";
            this.HammingWindowRadioButton.Size = new System.Drawing.Size(137, 21);
            this.HammingWindowRadioButton.TabIndex = 2;
            this.HammingWindowRadioButton.Text = "Hamming window";
            this.HammingWindowRadioButton.UseVisualStyleBackColor = true;
            // 
            // HannWindowRadioButton
            // 
            this.HannWindowRadioButton.AutoSize = true;
            this.HannWindowRadioButton.Location = new System.Drawing.Point(6, 48);
            this.HannWindowRadioButton.Name = "HannWindowRadioButton";
            this.HannWindowRadioButton.Size = new System.Drawing.Size(112, 21);
            this.HannWindowRadioButton.TabIndex = 1;
            this.HannWindowRadioButton.Text = "Hann window";
            this.HannWindowRadioButton.UseVisualStyleBackColor = true;
            // 
            // RectangularWindowRadioButton
            // 
            this.RectangularWindowRadioButton.AutoSize = true;
            this.RectangularWindowRadioButton.Checked = true;
            this.RectangularWindowRadioButton.Location = new System.Drawing.Point(6, 21);
            this.RectangularWindowRadioButton.Name = "RectangularWindowRadioButton";
            this.RectangularWindowRadioButton.Size = new System.Drawing.Size(155, 21);
            this.RectangularWindowRadioButton.TabIndex = 0;
            this.RectangularWindowRadioButton.TabStop = true;
            this.RectangularWindowRadioButton.Text = "Rectangular window";
            this.RectangularWindowRadioButton.UseVisualStyleBackColor = true;
            // 
            // ProcessingBar
            // 
            this.ProcessingBar.Location = new System.Drawing.Point(3, 188);
            this.ProcessingBar.Name = "ProcessingBar";
            this.ProcessingBar.Size = new System.Drawing.Size(700, 23);
            this.ProcessingBar.TabIndex = 5;
            // 
            // ProcessingLabel
            // 
            this.ProcessingLabel.Location = new System.Drawing.Point(3, 214);
            this.ProcessingLabel.Name = "ProcessingLabel";
            this.ProcessingLabel.Size = new System.Drawing.Size(700, 23);
            this.ProcessingLabel.TabIndex = 6;
            this.ProcessingLabel.Text = " ";
            // 
            // ProcesssingWorker
            // 
            this.ProcesssingWorker.WorkerReportsProgress = true;
            this.ProcesssingWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.ProcesssingWorker_DoWork);
            this.ProcesssingWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.ProcesssingWorker_ProgressChanged);
            this.ProcesssingWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.ProcesssingWorker_RunWorkerCompleted);
            // 
            // ClipCharacteristicControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.ProcessingLabel);
            this.Controls.Add(this.ProcessingBar);
            this.Controls.Add(this.FrequencyWindowBox);
            this.Controls.Add(this.FrequencyAnalizingWindowSettings);
            this.Name = "ClipCharacteristicControl";
            this.Size = new System.Drawing.Size(1268, 859);
            this.FrequencyAnalizingWindowSettings.ResumeLayout(false);
            this.FrequencyAnalizingWindowSettings.PerformLayout();
            this.FrequencyWindowBox.ResumeLayout(false);
            this.FrequencyWindowBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox FrequencyAnalizingWindowSettings;
        private System.Windows.Forms.Button ProcessButton;
        private System.Windows.Forms.Label ClipForLabel;
        private System.Windows.Forms.Label ClipFromLabel;
        private System.Windows.Forms.TextBox ClipFrom;
        private System.Windows.Forms.TextBox ClipFor;
        private System.Windows.Forms.Label ClipOverlayPercentageLabel;
        private System.Windows.Forms.TextBox ClipOverlayPercentage;
        private System.Windows.Forms.GroupBox FrequencyWindowBox;
        private System.Windows.Forms.RadioButton BarlettWindowRadioButton;
        private System.Windows.Forms.RadioButton BlackmanWindowRadioButton;
        private System.Windows.Forms.RadioButton HammingWindowRadioButton;
        private System.Windows.Forms.RadioButton HannWindowRadioButton;
        private System.Windows.Forms.RadioButton RectangularWindowRadioButton;
        private System.Windows.Forms.Label ClipFrameCountLabel;
        private System.Windows.Forms.TextBox ClipFrameCount;
        private System.Windows.Forms.CheckBox OneChannelCheckbox;
        private System.Windows.Forms.ProgressBar ProcessingBar;
        private System.Windows.Forms.Label ProcessingLabel;
        private System.ComponentModel.BackgroundWorker ProcesssingWorker;
    }
}
