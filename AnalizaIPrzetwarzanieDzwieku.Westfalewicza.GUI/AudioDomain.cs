﻿namespace AnalizaIPrzetwarzanieDzwieku.Westfalewicza.GUI
{
    public enum AudioDomain
    {
        None = 0,
        Time = 1,
        Frequency = 2,
        Spectogram = 3,
        Cepstrum = 4,
        DirectoryProcessing = 5,
    }
}